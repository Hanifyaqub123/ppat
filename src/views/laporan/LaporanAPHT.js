import React, { useState } from "react";
import {
  CTextarea,
  CContainer,
  CCol,
  CRow,
  CForm,
  CFormGroup,
  CLabel,
  CInput,
  CButton,
} from "@coreui/react";
import axios from 'axios'
import kecamatan from '../laporan/DataKecamatan'
import jenisHak from '../laporan/jenisHak'

const LaporanAPHT = () => {
  const year1 = (new Date()).getFullYear();
  const years = Array.from(new Array(5),( val, index) => index + year1);
  let myCurrentDate = new Date() 
  let month = ( '0' + (myCurrentDate.getMonth()+1) ).slice( -2 );
  let year = myCurrentDate.getFullYear();
  const today = year+"-"+month
  const regex = /[.,\s]/g;
  const user = JSON.parse(localStorage.getItem("user"))
  const [nik] = useState(user[0].nik);
  const [aktaNomor, setAktaNomor] = useState("");
  const [aktaTanggal, setAktaTanggal] = useState("");
  const [jenisPelayanan] = useState("APHT");
  const [namaPengalihan, setNamaPengalihan] = useState("");
  const [namaPenerima, setNamaPenerima] = useState("");
  const [luasBangunan, setLuasBangunan] = useState("");
  const [nilaiTransaksi, setNilaiTransaksi] = useState("");
  const [nopTahun, setNopTahun] = useState("");
  const [njop, setNjop] = useState("");
  const [sspTgl, setSspTgl] = useState("0");
  const [sspRp, setSspRp] = useState("0");
  const [ssbTgl, setSsbTgl] = useState("0");
  const [ssbRp, setSsbRp] = useState("0");
  const [ket, setKet] = useState("");
  const [bulanIni] = useState(today);
  const [namaPPAT] = useState(user[0].nama_lengkap)
  const [inputListJnsNoHak, setInputListJnsNoHak] = useState([{ jenis_hak: "", nomor_hak: "" }]);
  const [inputListAlamat, setInputListAlamat] = useState([{ jenis_kota: "", jenis_kecamatan: "",jenis_desa:"" }]);
  const [inputListLuasTanah, setInputListLuasTanah] = useState([{ luas_tanah: ""}]);
  const [inputListLuasBangunan, setInputListLuasBangunan] = useState([{ luas_bangunan: ""}]);

 

  // React.useEffect(()=>{
    // var tmp = []
    // inputList.map((res)=>{
    //   tmp.push(res.jenis_hak +"-"+ res.nomor_hak)
    // })
    // alert(tmp)
  // },[])

// Handle Data Jenis Dan No hak
  const handleAddClickJnsNoHak = () => {
    setInputListJnsNoHak([...inputListJnsNoHak, { jenis_hak: "", nomor_hak: "" }]);
  };
  const handleRemoveClickJnsNoHak = index => {
    const list = [...inputListJnsNoHak];
    list.splice(index, 1);
    setInputListJnsNoHak(list);
  };
  const handleInputChangeJnsNoHak = (e, index) => {
    const { name, value } = e.target;
    const list = [...inputListJnsNoHak];
    list[index][name] = value;
    setInputListJnsNoHak(list);
  };

  // Handle Data Jenis Dan No hak

  // Handle Alamat
  const handleAddClickAlamat = () => {
    setInputListAlamat([...inputListAlamat, { jenis_kota: "", jenis_kecamatan: "",jenis_desa:"" }]);
  };
  const handleRemoveClickAlamat = index => {
    const list = [...inputListAlamat];
    list.splice(index, 1);
    setInputListAlamat(list);
  };
  const handleInputChangeAlamat = (e, index) => {
    const { name, value } = e.target;
    const list = [...inputListAlamat];
    list[index][name] = value;
    setInputListAlamat(list);
  };

    // Handle Alamat

     // Handle Luas Tanah
  const handleAddClickLuasTanah = () => {
    setInputListLuasTanah([...inputListLuasTanah, { luas_tanah: ""}]);
  };
  const handleRemoveClickLuasTanah = index => {
    const list = [...inputListLuasTanah];
    list.splice(index, 1);
    setInputListLuasTanah(list);
  };
  const handleInputChangeLuasTanah = (e, index) => {
    const { name, value } = e.target;
    const list = [...inputListLuasTanah];
    list[index][name] = value;
    setInputListLuasTanah(list);
  };

    // Handle Luas Tanah

     // Handle Luas Bangunan
  const handleAddClickLuasBangunan = () => {
    setInputListLuasBangunan([...inputListLuasBangunan, { luas_bangunan: ""}]);
  };
  const handleRemoveClickLuasBanguan = index => {
    const list = [...inputListLuasBangunan];
    list.splice(index, 1);
    setInputListLuasBangunan(list);
  };
  const handleInputChangeLuasBangunan = (e, index) => {
    const { name, value } = e.target;
    const list = [...inputListLuasBangunan];
    list[index][name] = value;
    setInputListLuasBangunan(list);
  };

    // Handle Luas Bangunan


  const dataArgomulyo = [
    {
      desa : "Cebongan"
    },
    {
      desa : "Kumpulrejo"
    },
    {
      desa : "Ledok"
    },
    {
      desa : "Noborejo"
    },
    {
      desa : "Randuacir"
    },
    {
      desa : "Tegalrejo"
    }
  ]
 
  const dataSidoMukti = [
    {
      desa : "Dukuh"
    },
    {
      desa : "Kalicacing"
    },
    {
      desa : "Kecandran"
    },
    {
      desa : "Mangunsari"
    }
  ]

  const dataSidorejo = [
    {
      desa : "Blotongan"
    },
    {
      desa : "Bugel"
    },
    {
      desa : "Kauman Kidul"
    },
    {
      desa : "Pulutan"
    },
    {
      desa : "Salatiga"
    },
    {
      desa : "Sidorejo Lor"
    }
  ]
  const dataTingkir = [
    {
      desa : "Gendongan"
    },
    {
      desa : "Kalibening"
    },
    {
      desa : "Kutowinangun"
    },
    {
      desa : "Kutowinangun Kidul"
    },
    {
      desa : "Kutowinangun Lor"
    },
    {
      desa : "Sidorejo Kidul"
    },
    {
      desa : "Tingkir Lor"
    },
    {
      desa : "Tingkir Tengah"
    }
  ]
  
  const handleNomorAkta = (e) => {
    setAktaNomor(e.target.value);
  };

  const handleTanggalAkta = (e) => {
    setAktaTanggal(e.target.value);
  };

  const handleNamaPengalih = (e) => {
    setNamaPengalihan(e.target.value);
  };

  const handleNamaPenerima = (e) => {
    setNamaPenerima(e.target.value);
  };

  const handleLuasBangunan = (e) => {
    setLuasBangunan(e.target.value);
  };

  const handleNilaiTransaksi = (e) => {
    setNilaiTransaksi(e.target.value);
  };

  const handleNOPTahun = (e) => {
    setNopTahun(e.target.value);
  };

  const handleNJOP = (e) => {
    setNjop(e.target.value);
  };

  const handleSSPTanggal = (e) => {
    setSspTgl(e.target.value);
  };

  const handleSSPRp = (e) => {
    setSspRp(e.target.value);
  };

  const handleSSBTanggal = (e) => {
    setSsbTgl(e.target.value);
  };

  const handleSSBRp = (e) => {
    setSsbRp(e.target.value);
  };

  const handleKeterangan = (e) => {
    setKet(e.target.value);
  };

 

  const handleSimpan = (e) => {
    // alert(Valuejenishak+"-"+jenisNoHak)
    // alert(Valuekota+","+Valuekecamatan+","+Valuedesa)

    var jenisNoHak = []
    inputListJnsNoHak.map((res)=>{
      jenisNoHak.push(res.jenis_hak +"-"+ res.nomor_hak)
    })

    // kosong tanda -

    var alamat = []
    inputListAlamat.map((res)=>{
      alamat.push(res.jenis_kota +"-"+ res.jenis_kecamatan+"-"+res.jenis_desa)
    })

    // ksosong tanda --

    var luasTanah = []
    inputListLuasTanah.map((res)=>{
      luasTanah.push(res.luas_tanah)
    })

    // kosong tanda ""

    var luasBangunan = []
    inputListLuasBangunan.map((res)=>{
      luasBangunan.push(res.luas_bangunan)
    })

    // kosong tanda ""



    const post = {
      nik : nik,
      akta_nomor : aktaNomor,
      akta_tanggal: aktaTanggal,
      bentuk_perbuatan_hukum : jenisPelayanan,
      nama_pengalihan : namaPengalihan,
      nama_penerima : namaPenerima,
      jenis_dan_nomor_hak: jenisNoHak.toString(),
      letak_tanah_dan_bangunan: alamat.toString(),
      luas_tanah : luasTanah.toString(),
      luas_bangunan: luasBangunan.toString(),
      nilai_transaksi : nilaiTransaksi.replace(regex,''),
      nop_tahun : nopTahun,
      njop_rp : njop.replace(regex,''),
      ssp_tanggal : sspTgl,
      ssp_rp : sspRp.replace(regex,''),
      ssb_tanggal : ssbTgl,
      ssb_rp : ssbRp.replace(regex,''),
      ket:ket,
      bulan_ini:bulanIni,
      nama_ppat:namaPPAT
    }

  if (aktaNomor === "") {
      alert("Akta Nomor Tidak boleh kosong beri tanda - jika kosong")
  } else if (aktaTanggal === "") {
    alert("Akta Tanggal Tidak boleh kosong beri tanda - jika kosong")
  }else if (jenisPelayanan === "") {
    alert("Tidak boleh kosong beri tanda - jika kosong")
  }else if (namaPengalihan === "") {
    alert("Pihak Pengalihkan Tidak boleh kosong beri tanda - jika kosong")
  }else if (namaPenerima === "") {
    alert("Pihak Penerima Tidak boleh kosong beri tanda - jika kosong")
  }else if (jenisNoHak.toString() === "-") {
    alert("Jenis dan Nomor Hak Tidak boleh kosong")
  }else if (alamat.toString() === "--") {
    alert("Letak Tanah dan Bangunan Tidak boleh kosong")
  }else if (luasTanah.toString() === "") {
    alert("Luas Tanah Tidak boleh kosong")
  }else if (luasBangunan.toString() === "") {
    alert("Luas Bangunan Tidak boleh kosong ")
  }else if (nilaiTransaksi === "") {
    alert("Nilai Transaksi Tidak boleh kosong beri 0 jika kosong")
  }else if (nilaiTransaksi === "-") {
    alert("Nilai Transaksi Tidak boleh menggunakan - , gunakan 0 jika kosong")
  }else if (nopTahun === "") {
    alert("Nop Tahun Tidak boleh kosong beri tanda - jika kosong")
  }else if (njop === "") {
    alert("NJOP Tidak boleh kosong beri 0 jika kosong")
  }else if (njop === "-") {
    alert("NJOP Tidak boleh  menggunakan - , gunakan 0 jika kosong")
  }else if (sspTgl === "") {
    alert("SSP Tanggal Tidak boleh kosong beri tanda - jika kosong")
  }else if (sspRp === "") {
    alert("SSP Rp Tidak boleh kosong beri 0 jika kosong")
  }else if (sspRp === "-") {
    alert("SSP Rp Tidak boleh menggunakan - , gunakan 0 jika kosong")
  }else if (ssbTgl === "") {
    alert("SSB Tanggal Tidak boleh kosong beri tanda - jika kosong")
  }else if (ssbRp === "") {
    alert("SSB Rp Tidak boleh kosong beri 0 jika kosong")
  }else if (ssbRp === "-") {
    alert("SSB Rp Tidak boleh menggunakan - , gunakan 0 jika kosong")
  }else if (ket === "") {
    alert("Keterangan Tidak boleh kosong beri tanda - jika kosong")
  }else{
    // alert(JSON.stringify(post))
    axios.post(""+window.server+"rest-api/ppat/laporan/laporanbulanan.php",post)
    .then((res)=>{
      alert(JSON.stringify(res.data))
      window.location.reload();
    },(err)=>{
      console.log(err)
    })
   
  }
  
  };
  



  return (
    <>
      <CContainer fluid>
        <CRow>
          <CCol sm="6">
            <CForm action="" method="post">
              <CFormGroup>
                <CLabel>Nomor Akta</CLabel>
                <CInput
                  defaultValue={aktaNomor}
                  type="text"
                  id="no-akta"
                  name="no-akta"
                  placeholder="Nomor Akta / Tahun"
                  onChange={handleNomorAkta}
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Tanggal Akta</CLabel>
                <CInput
                  defaultValue={aktaTanggal}
                  type="date"
                  id="tanggal-akta"
                  name="tanggal-akta"
                  placeholder="Tanggal Akta . . ."
                  onChange={handleTanggalAkta}
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Bentuk Perbuatan Hukum</CLabel>
                <CInput
                  disabled="disabled"
                  defaultValue={jenisPelayanan}
                  type="text"
                  id="jual-beli"
                  name="jual-beli"
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Pihak Yang Mengalihkan</CLabel>
                <CTextarea
                  defaultValue={namaPengalihan}
                  rows="5"
                  id="nama-pengalih"
                  name="nama-pengalih"
                  placeholder="1. .... , 2. ..... atau ..... , ...."
                  onChange={handleNamaPengalih}
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Pihak Yang Menerima</CLabel>
                <CTextarea
                  defaultValue={namaPenerima}
                  rows="5"
                  id="nama-penerima"
                  name="nama-penerima"
                  placeholder="1. .... , 2. ..... atau ..... , ...."
                  onChange={handleNamaPenerima}
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Jenis dan Nomor Hak</CLabel>
               {
                inputListJnsNoHak.map((x,i)=>{
                  return(
                    <div >
                    <div style={{marginBottom:'3%',display:'flex',flexDirection:'row'}}>

                    <select value={x.jenis_hak} name="jenis_hak" onChange={e => handleInputChangeJnsNoHak(e, i)} style={{paddingTop:5,paddingBottom:5,paddingLeft:5,borderWidth:0,borderRadius:'10%'}}>
                              <option value="" selected="selected" >--- Jenis Hak ---</option>
                              {
                                jenisHak.map((res,index)=>{
                                  return(
                                    <option key={index} value={res.jenis}>{res.jenis}</option>
                                  )
                                })
                              }
                    </select>
                            
                    <CInput
                          style={{marginLeft:10}}
                          value={x.nomor_hak}
                          type="number"
                          name="nomor_hak"
                          placeholder="000"
                          onChange={e => handleInputChangeJnsNoHak(e, i)}
                        />

                    {inputListJnsNoHak.length - 1 === i &&  <CButton onClick={handleAddClickJnsNoHak}   style={{marginLeft:10}} color="info" className="px-4">+</CButton> }
                    {inputListJnsNoHak.length !== 1 &&   <CButton style={{marginLeft:10}} onClick={() => handleRemoveClickJnsNoHak(i)} color="danger" className="px-4">-</CButton>} 
                    
                    </div>        
                  </div>
            
                  )
                })
        
              }

            </CFormGroup>
              <CFormGroup>
                <CLabel>Letak Tanah dan Bangunan</CLabel>
                {
                  inputListAlamat.map((x,i)=>{
                    return(
                      <div style={{display:'flex', flexDirection:'row',marginTop:10}}>
                      <select name="jenis_kota" value={x.jenis_kota} onChange={e => handleInputChangeAlamat(e, i)} style={{paddingTop:10,paddingBottom:10,paddingLeft:5,borderWidth:0,borderRadius:'5%'}}>
                        <option value="" selected="selected" >---Kabupaten/Kota---</option>
                        <option value="Salatiga">Salatiga</option>
                      </select>
                      
                      <select name="jenis_kecamatan"  value={x.jenis_kecamatan} onChange={e => handleInputChangeAlamat(e, i)} style={{marginLeft:"2%",borderWidth:0,borderRadius:'5%'}}>
                        <option value="" selected="selected" >---Kecamatan---</option>
                        {
                          kecamatan.map((res,index)=>{
                            return(
                              <option key={index} value={res.kecamatan}>{res.kecamatan}</option>
                            )
                          })
                        }
                      </select>
                      <select  name="jenis_desa" value={x.jenis_desa} onChange={e => handleInputChangeAlamat(e, i)} style={{marginLeft:"1%",borderWidth:0,borderRadius:'5%'}}>
                        <option value="" selected="selected" >---Desa/kelurahan---</option>
                        {
                          x.jenis_kecamatan === "Argomulyo" &&
                          dataArgomulyo.map((res,index)=>{
                            return(
                              <option key={index} value={res.desa} >{res.desa}</option>
                            )
                          })
                        }
                         {
                           x.jenis_kecamatan === "Sidomukti" &&
                          dataSidoMukti.map((res,index)=>{
                            return(
                              <option key={index} value={res.desa} >{res.desa}</option>
                            )
                          })
                        } 
                        {
                           x.jenis_kecamatan === "Sidorejo" &&
                          dataSidorejo.map((res,index)=>{
                            return(
                              <option key={index} value={res.desa} >{res.desa}</option>
                            )
                          })
                        } 
                        {
                           x.jenis_kecamatan === "Tingkir" &&
                          dataTingkir.map((res,index)=>{
                            return(
                              <option key={index} value={res.desa} >{res.desa}</option>
                            )
                          })
                        } 
                      </select>
                      {inputListAlamat.length - 1 === i &&  <CButton onClick={handleAddClickAlamat}   style={{marginLeft:10}} color="info" className="px-4">+</CButton> }
                      {inputListAlamat.length !== 1 &&   <CButton style={{marginLeft:10}} onClick={() => handleRemoveClickAlamat(i)} color="danger" className="px-4">-</CButton>} 
                    
                  </div>
                
                    )
                  })
                }
             
              </CFormGroup>
              <CFormGroup>
                <CLabel>Luas Tanah</CLabel>
                {
                  inputListLuasTanah.map((x,i)=>{
                    return(
                      < div style={{display:"flex", flexDirection:'row',marginTop:10}}>
                      <CInput
                      value={x.luas_tanah}
                      type="number"
                      name="luas_tanah"
                      placeholder="00"
                      onChange={e => handleInputChangeLuasTanah(e, i)}
                      />
                    {inputListLuasTanah.length - 1 === i &&  <CButton onClick={handleAddClickLuasTanah}   style={{marginLeft:10}} color="info" className="px-4">+</CButton> }
                    {inputListLuasTanah.length !== 1 &&   <CButton style={{marginLeft:10}} onClick={() => handleRemoveClickLuasTanah(i)} color="danger" className="px-4">-</CButton>} 
                  </div>
                    )
                  })
                }
              
              </CFormGroup>
              <CFormGroup>
                <CLabel>Luas Bangunan</CLabel>
                {
                  inputListLuasBangunan.map((x,i)=>{
                    return(
                      < div style={{display:"flex", flexDirection:'row',marginTop:10}}>
                         <CInput
                          value={x.luas_bangunan}
                          type="number"
                          name="luas_bangunan"
                          placeholder="00"
                          onChange={e => handleInputChangeLuasBangunan(e, i)}
                          />
                    {inputListLuasBangunan.length - 1 === i &&  <CButton onClick={handleAddClickLuasBangunan}   style={{marginLeft:10}} color="info" className="px-4">+</CButton> }
                    {inputListLuasBangunan.length !== 1 &&   <CButton style={{marginLeft:10}} onClick={() => handleRemoveClickLuasBanguan(i)} color="danger" className="px-4">-</CButton>} 
                      </ div>
                    )
                  })
                }
               
              </CFormGroup>
            </CForm>
          </CCol>
          <CCol sm="6">
            <CForm action="" method="post">
              <CFormGroup>
                <CLabel>Nilai Transaksi</CLabel>
                <CInput
                  defaultValue={nilaiTransaksi}
                  type="number"
                  id="nilai-transaksi"
                  name="nilai-transaksi"
                  placeholder="000.000.000"
                  onChange={handleNilaiTransaksi}
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>NOP / Tahun</CLabel>
                <CInput
                  defaultValue={nopTahun}
                  type="text"
                  id="nop-tahun"
                  name="nop-tahun"
                  placeholder="xx.xx.xxx.xxx-xxxx.0/2021"
                  onChange={handleNOPTahun}
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>NJOP</CLabel>
                <CInput
                  defaultValue={njop}
                  type="number"
                  id="njop"
                  name="njop"
                  placeholder="00.0000.000"
                  onChange={handleNJOP}
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>SSP Tanggal</CLabel>
                <CInput
                  disabled
                  defaultValue={sspTgl}
                  type="date"
                  id="ssp-tanggal"
                  name="ssp-tanggal"
                  onChange={handleSSPTanggal}
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>SSP Rp</CLabel>
                <CInput
                  disabled
                  defaultValue={sspRp}
                  type="number"
                  id="ssp-rp"
                  name="ssp-rp"
                  placeholder="00.0000.000"
                  onChange={handleSSPRp}
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>SSB Tanggal</CLabel>
                <CInput
                   disabled
                  defaultValue={ssbTgl}
                  type="date"
                  id="ssb-tanggal"
                  name="ssb-tanggal"
                  onChange={handleSSBTanggal}
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>SSB Rp</CLabel>
                <CInput
                   disabled
                  defaultValue={ssbRp}
                  type="number"
                  id="ssb-rp"
                  name="ssb-rp"
                  placeholder="00.0000.000"
                  onChange={handleSSBRp}
                />
              </CFormGroup>
              <CFormGroup>
                <CLabel>Keterangan</CLabel>
                <CInput
                  defaultValue={ket}
                  type="text"
                  id="keterangan"
                  name="keterangan"
                  placeholder="-"
                  onChange={handleKeterangan}
                />
              </CFormGroup>
              {/* <select
        onChange={handleMonth}
        style={{
          width: "40%",
          height: 40,
          paddingLeft: 5,
          marginRight: "2%",
          border: "none",
          borderRadius: 5,
          marginBottom: 10,
        }}
        id="month"
        name="month"
      >
        <option selected="selected">Pilih Bulan</option>
        {month1.map((res, index) => {
          return (
            <option key={index} value={res.nilai}>
              {res.bulan}
            </option>
          );
        })}
      </select>
       */}
      {/* <select
        onChange={handleYears}
        style={{
          width: "40%",
          height: 40,
          paddingLeft: 5,
          marginRight: "3%",
          border: "none",
          borderRadius: 5,
          marginBottom: 10,
        }}
        id="month"
        name="month"
      >
        <option selected="selected">Pilih Tahun</option>
        {years.map((res, index) => {
          return (
            <option key={index} value={res}>
              {res}
            </option>
          );
        })}
      </select> */}

              <CCol xs="13" className="text-right">
                <CButton onClick={handleSimpan} color="success">
                  Simpan
                </CButton>
              </CCol>
            </CForm>
          </CCol>
        </CRow>
      </CContainer>
    </>
  );
};

export default LaporanAPHT;
